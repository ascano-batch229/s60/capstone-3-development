import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function Login(){

	// Allow us to consume User Context object and its properties
	const {user, setUser} = useContext(UserContext);

	const navigate = useNavigate();

	// State Hooks - Store values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	

	const [isActive, setIsActive] = useState(false)

	console.log(email);
	console.log(password1);


	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		if(email !== '' && password1 !== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password1])

	// Function to simulate user registration

	function registerUser(e){

		// Prevents page redirection via form submission
		e.preventDefault();

		// Process a fetch request to the corresponding backend API
		/*
			SYNTAX: 
				fetch('url', {options})
		*/
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {

			// It is good practice to always to print out the result of our fetch request to ensure that the correct information is received in our frontend application.
			console.log(data)
			// If no user information is found, the "access" property will not be available and will return undefined
	        // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
		if(typeof data.access !== "undefined"){
			localStorage.setItem('token', data.access);

			retrieveUserDetails(data.access);

			Swal.fire({
				title: "Login Succesfull",
				icon: "success",
				text: "Welcome to the store"
			})

			navigate("/courses");
		}else{
			Swal.fire({
				title: "Authentication Failed",
				icon: "error",
				text: "Check your login detail and try again!"			
			})

		}

		})

		setEmail('');
		setPassword1('');
		
		// Set the email of the authenticated user in the local storage
		/*
		
			SYNTAX:
				localStorage.setIem('propertyName', value);

		*/

		// localStorage.setItem('email', email);

		// setUser({
		// 	email: localStorage.getItem('email')
		// })

	

	}

	const retrieveUserDetails = (token) => {
		// The token will be sent as part of the request's header information
		// We put "Bearer" in front of the token to follow implementation standards for JWTs
		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => {
			console.log(res);
			return res.json();
		})
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
		.catch(error => {
			console.error(error);
		});

	}



	return(

		(user.id !== null ) ?

			<Navigate to="/courses"/>
			:

		<Form onSubmit={(e) => registerUser(e)}>

			<Form.Group className="mb-3" controlId="userEmail">
				<h1>Login</h1>
				<Form.Label>Email address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>

			</Form.Group>

			<Form.Group className="mb-3" controlId="password1" >
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Password"  value={password1} onChange={e => setPassword1(e.target.value)} required/>
			</Form.Group>


				{/*Conditional Rendering -> IF ACTIVE BUTTON IS CLICKABLE -> IF INACTIVE BUTTON IS NOT CLICKABLE*/}
				{
					(isActive)?
					<Button variant="primary" type="submit" controlId="submitBtn">
						Login
					</Button>
					:
					<Button variant="primary" type="submit" controlId="submitBtn" disabled>
						Login
					</Button>										

				}
		</Form>


		) 


}