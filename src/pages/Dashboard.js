import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';



export default function AddProduct (){

	const [productName, setProductname] = useState('');
	const [description, setDescription] = useState('');
	const [availableInStock, setAvailableInStock] = useState('');
	const [price, setPrice] = useState('');

	const [isActive, setIsActive] = useState(false)



	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		if(productName !== '' && description !== '' && availableInStock !== '' && price !== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},  [productName, description, availableInStock, price])



	function productAdd(e){

		e.preventDefault();

		fetch('http://localhost:4000/products/NewProduct', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				availableInStock: availableInStock,
				price: price
	
			})
			})
			.then(res => res.json())
			.then(data => {
			console.log(data)

			if(data === true){
			Swal.fire({
				title: "Product Successfully Added!",
				icon: "success",
				text: "Product Successfully added to the shop"
			})	

			}else{
			Swal.fire({
				title: "Unable to add product",
				icon: "error",
				text: "Check your details and try again!"			
				})

			}

		})

	}





	return(

 	
 		<Form onSubmit={(e) => productAdd(e)} >
	 		<Form.Group>
	 			<h2> Add new Products	</h2>
	 		</Form.Group>	

	 		<Form.Group className="m-3" >		
	      		<Form.Label> Product Name </Form.Label>
	      		<Form.Control type="productName" placeholder="Product Name" value={productName} onChange={e => setProductname(e.target.value)} required />
	      </Form.Group>

	 		<Form.Group className="m-3">		
	      		<Form.Label> Product Description </Form.Label>
	      		<Form.Control type="description" placeholder="Product Description" value={description} onChange={e => setDescription(e.target.value)} required />
	      </Form.Group>

	       	<Form.Group className="m-3">		
	      		<Form.Label> In Stock</Form.Label>
	      		<Form.Control type="availableInStock" placeholder="In Stock" value={availableInStock} onChange={e => setAvailableInStock(e.target.value)} required />
	      </Form.Group>

	       	<Form.Group className="m-3">		
	      		<Form.Label> Price</Form.Label>
	      		<Form.Control type="price" placeholder="Price" value={price} onChange={e => setPrice(e.target.value)} required />
	      </Form.Group>

	    	<Form.Group className="m-3">
	    	{
	    		(isActive) ?
	    		<Button variant="primary" type="submit" controlId="submitBtn"> Add product </Button>
	    		:
	    		<Button variant="primary" type="submit" controlId="submitBtn" disabled> Add product </Button>
	    	}
					
			</Form.Group>
		</Form>

    

		)



}