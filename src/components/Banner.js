// Destructuring components for cleaner codebase
import {Button, Row, Col} from 'react-bootstrap';


export default function	Banner(){
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1> CAPSTONE THREE STORE </h1>
				<p> WEBSITE UNDER CONSTRUCTION! </p>
				<Button variant="primary"> UNAVAILABLE </Button>
			</Col>
		</Row>
		)
}