import {Card, Button} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom';


export default function CourseCard({courseProps}){
	// Checks to see if the data was sucessfully passed
	console.log(courseProps);
	console.log(typeof courseProps);

	// Desctructuring the data to avoid Dot notation
	const {name, description, price, _id, productName, availableInStock} = courseProps;

	// 3 Hooks in react
	// 1. useState
	// 2. useEffect
	// 3. useContext

	// use the useState hook for the component to be able to store 'state(condition)'
	// States are used to keep tracl of information related to individual components
	// Syntax -> const [getter, setter] = useState(initialGetterValue); 


	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	console.log(useState(0));

// 	function enroll(){

// /*		if(seats === 1){
// 			alert("No seats Available!")
// 		}

// 		setCount(count + 1);
// 		setSeats(seats - 1);
// 		console.log("Enrollees" + count);*/

// 		if(seats > 0){
// 			setCount(count + 1);
// 			setSeats(seats - 1);
// 		}else{
// 			alert("No more seats Available!");
// 		}


// 	}




	return(

		<Card className="my-3">
			<Card.Body>
				<Card.Title>{productName}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Card.Text> Enrollees: {count}</Card.Text>
				<Card.Text> In stock: {availableInStock}</Card.Text>
				<Link className="btn btn-primary" to={`/courseView/${_id}`}> Details </Link>
			</Card.Body>
		</Card>  
		)
}