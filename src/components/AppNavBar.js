import {useContext} from 'react';
import NavBar from 'react-bootstrap/NavBar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext.js'


export default function	AppNavBar(){

	// const[user, setUser] =useState(localStorage.getItem('email'));

	const {user} = useContext(UserContext);

	return (
		<NavBar bg="light" expand="lg" className="p-3">
			<NavBar.Brand as={NavLink} exact to="/"> CAPSTONE </NavBar.Brand>
			<NavBar.Toggle aria-controls="basic-navbar-nav"/> 
			<NavBar.Collapse id="basic-navbar-nav">	
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} exact to="/"> Home </Nav.Link>
					<Nav.Link as={NavLink} exact to="/courses"> Products</Nav.Link>

					
					{
					
						(user.id && user.isAdmin !== undefined) ?

						(
							<>
								<Nav.Link as={NavLink} exact to="/logout"> Logout </Nav.Link>
								<Nav.Link as={NavLink} exact to="/dashboard"> Admin Dashboard</Nav.Link>
								
							</>
						)
						:
						(
							<>
								<Nav.Link as={NavLink} exact to="/login"> Login </Nav.Link>
								<Nav.Link as={NavLink} exact to="/register"> Register </Nav.Link>
							</>
						)
					}					
				</Nav>

			</NavBar.Collapse>
		</NavBar>


		)
}